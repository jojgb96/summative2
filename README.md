# summative2
aplikasi ini dibuat dengan html dan javascript dimana akan menerima 2 inputan
dari kata yang akan ditebak dan huruf yang sudah dimasukan untuk menebak kata
dibawah nya juga ada clue untuk menebak kata nya , input nya menggunakan button
yang banyak 

berikut dibawah ini adalah contoh tampilan utamanya![hangman](hangman.PNG)

kita harus menekan salah satu huruf untuk menebak kata yang dimaksud ,jika tebakan salah

maka akan masuk kata dan tebakan sejauh ini , sementara jika benar maka hanya akan masuk salah satu hurufnya ke kata ,disini player akan diberikan 5 nyawa / 5 kali kesempatan

jika nyawa / kesempatan sudah habis maka akan diberi peringatan seperti dibawah ini

![kalah](kalah.PNG)

dan jika kita menang dan berhasil menebak seluruh huruf maka akan muncul tampilan seperti 

dibawah ini

![menang](menang.PNG)

